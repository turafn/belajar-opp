<?php 
require_once 'animal.php';
$sheep = new Animal("shaun",2,"mbeeee");

echo $sheep->get_name();
echo "<br>";
echo $sheep->get_legs();
echo "<br>";
echo $sheep->get_cold_bloded();
echo "<br><br>";

$ape = new Ape('kerasakti', 2,"auooooo");
echo $ape->get_name();
echo "<br>";
echo $ape->get_legs();
echo "<br>";
echo $ape->get_yell();
echo "<br><br>";

$frog = new Frog('buduk', 2,"hop hop");
echo $frog->get_name();
echo "<br>";
echo $frog->get_legs();
echo "<br>";
echo $frog->get_cold_bloded();
echo "<br><br>";